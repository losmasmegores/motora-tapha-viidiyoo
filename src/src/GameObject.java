package src;

import componentes.XLR8;

import java.util.ArrayList;
import java.util.Collection;

public class GameObject{
    private static int idGo = 0;
    private int id;
    public Collection<XLR8> components = new ArrayList<>();
//    public boolean wantsToDestroy = false;
    public GameObject(){
        id = idGo++;
//        System.out.println("Estoy vivo y soy el GO: "+idGo);

    }
    public int getId(){
        return  id;
    }
    public void addComponent(XLR8 component) {
        if (components.contains(component)) {
            return;
        }
        else {
            component.GameObjectID = this.getId();
            components.add(component);
        }
    }
    public <T extends XLR8> T getComponent(Class<T> type) {
        for (XLR8 component : components) {
            if (type.isInstance(component)) {
                return type.cast(component);
            }
        }
        return null;
    }

    public void removeComponent(XLR8 component) {
        components.remove(component);
    }
    public boolean hasComponent(Class<? extends XLR8> type) {
        for (XLR8 component : components) {
            if (type.isInstance(component)) {
                return true;
            }
        }
        return false;
    }
    public void update() {
        for (XLR8 component : components) {
            component.update();
        }
        quierenDestruir();
    }

    public void start() {
//        System.out.println("Starting game object " + idGo);
        for (XLR8 component : components) {
            component.start();
        }
    }

    public void onDestroy(){
        for (XLR8 component: components){
            component.onDestroy();
        }
//        GameLoop.getInstance().removeGameObject(this);
//        wantsToDestroy = true;
    }
    public void quierenDestruir(){
        for (XLR8 component: components){
            if(component.DestroyGameObject){
                GameLoop.getInstance().removeGameObject(this);
            }
        }
    }

}
