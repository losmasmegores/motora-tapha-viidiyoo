package src;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class GameLoop {
    private static GameLoop instance = null;
    public long deltaTime = 0;
    private final int fps;

    private LinkedList<GameObject> listaRemove = new LinkedList<GameObject>();
    private LinkedList<GameObject>listaStart = new LinkedList<GameObject>();
    private ArrayList<GameObject>listaUpdate = new ArrayList<GameObject>();

    private GameLoop(int fps) {
        this.fps = fps;
    }

    public static synchronized GameLoop getInstance()
    {

        if(instance==null)
            return instance = new GameLoop(60);
        else{
//            System.out.println("La instància ja ha estat creada");
            return instance;
        }

    }

    public void init() {
//        System.out.println("Starting gameObjects");
//        for (GameObject go : listaStart) {
//            go.start();
//            listaUpdate.add(go);
//        }
//        listaStart.clear();
        System.out.println("------------------------------START-----------------------------");
        while(!listaStart.isEmpty()){
            GameObject go = listaStart.poll();
            go.start();
            listaUpdate.add(go);
        }
        System.out.println("--------------------------------------------------------------");
    }
    public void deleteObjects(){
//        for (GameObject go : listaRemove) {
//            go.onDestroy();
//            listaUpdate.remove(go);
//        }
//        listaRemove.clear();
        System.out.println("------------------------------REMOVE-----------------------------");
        while(!listaRemove.isEmpty()){
            GameObject go = listaRemove.poll();
            go.onDestroy();
            if(listaUpdate.contains(go))listaUpdate.remove(go);
        }
        System.out.println("--------------------------------------------------------------");
    }
    public void update(){
        System.out.println("------------------------------UPDATE-----------------------------");
        for (GameObject go : listaUpdate) {
            go.update();
        }
        System.out.println("--------------------------------------------------------------");
    }
    public void updateFinal(){
        init();
        update();
        deleteObjects();
    }
    public boolean addGameObject(GameObject go){
        listaStart.offer(go);
        return false;
    }

    public boolean removeGameObject(GameObject go){
        if(listaRemove.contains(go))return true;
        else{
            listaRemove.add(go);
            return false;
        }
    }
//    public static void wantToDestroy(){
//        for (GameObject g : listaUpdate) {
//            if(g.wantsToDestroy){
//                listaRemove.add(g);
//            }
//        }
//    }
    public void duerme(long ms) {
        try {
            if(ms < 0) ms = 0;
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public static void print(String message)
    {
        String hora = java.time.LocalTime.now().toString().substring(0, 8);
        System.out.println("[" + hora + "] " + message);
    }
    public void instantiate(GameObject go){
        listaStart.add(go);
    }

    public void run() {
        long ultimoFrame = System.currentTimeMillis();
        while (true) {
            long frameActual = System.currentTimeMillis();
            deltaTime = frameActual-ultimoFrame;
            ultimoFrame = frameActual;
//            isFinal();
//            input();
            updateFinal();
//            render();
            long acabe = System.currentTimeMillis();
            long dormirTime = 1000 / fps - (acabe - frameActual);
            duerme(dormirTime);
        }
    }


}
