package src;

import componentes.Spawner;
import tools.MortiferoFactory;

public class Main {
    public static void main(String[] args) {
        GameLoop gl = GameLoop.getInstance();
        GameObject spawner = new GameObject();
        spawner.addComponent(new Spawner(new MortiferoFactory(), 1000));
        gl.instantiate(spawner);
        gl.run();
    }

}
