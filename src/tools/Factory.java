package tools;

public interface Factory<T> {
    public T create();
}
