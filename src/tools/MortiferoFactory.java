package tools;

import componentes.Mortifero;
import src.GameObject;

public class MortiferoFactory implements  Factory<GameObject>{
    @Override
    public GameObject create() {
        GameObject go = new GameObject();
        go.addComponent(new Mortifero(4));
        return go;
    }
}
