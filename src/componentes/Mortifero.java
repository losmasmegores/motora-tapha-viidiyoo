package componentes;


import src.GameLoop;
import src.GameObject;
import tools.MortiferoFactory;

public class Mortifero extends XLR8 {


    public int segundos;
    public long timeElapsed = 0;

    public Mortifero(int segundos){
        this.segundos = segundos;
    }
    @Override
    public void start() {
        GameLoop.print("GameObject "+this.GameObjectID+" : Mortifero Start");
    }
    @Override
    public void update() {

        GameLoop.print("GameObject "+this.GameObjectID+" : Mortifero restando, quedan " + segundos + " segundos");
        timeElapsed += GameLoop.getInstance().deltaTime;
        if(timeElapsed >= 1000 && segundos > 0){
            segundos--;
            timeElapsed = 0;
        }
        else if(segundos == 0){
            GameLoop.print("GameObject "+this.GameObjectID+" : Mortifero muerto");
            this.DestroyGameObject = true;
        }

    }
    @Override
    public void onDestroy() {
        GameLoop.print("GameObject "+this.GameObjectID+" : Mortifero Destroy");
    }
}
