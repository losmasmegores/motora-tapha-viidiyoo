package componentes;

import componentes.Mortifero;
import componentes.XLR8;
import src.GameLoop;
import src.GameObject;
import tools.Factory;

public class Spawner extends XLR8 {

    Factory<GameObject> factory;

    long timeToSpawn;
    long timeElapsed = 0;

    public Spawner(Factory<GameObject> factory, long tts) {
        this.factory = factory;
        timeToSpawn = tts;
    }

    @Override
    public void update() {
        timeElapsed += GameLoop.getInstance().deltaTime;
        if (timeElapsed >= timeToSpawn) {

            GameLoop.getInstance().instantiate(factory.create());
            timeElapsed -= timeToSpawn;
        }
    }

}
