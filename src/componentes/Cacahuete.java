package componentes;

import src.GameLoop;

public class Cacahuete extends XLR8 {
    @Override
    public void start() {
        GameLoop.print("Cacahuete Start");
    }

    @Override
    public void update() {
        GameLoop.print("Cacahuete Update");
    }

    @Override
    public void onDestroy() {
        GameLoop.print("Cacahuete Destroy");
    }
}
